package cz.vsmie.tomanek.gwt.client.event;

import java.io.Serializable;

import lombok.Getter;

import com.google.gwt.event.shared.GwtEvent;

public class OpenPageEvent extends GwtEvent<OpenPageHandler> {
	public static Type<OpenPageHandler> TYPE = new Type<OpenPageHandler>();

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<OpenPageHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(OpenPageHandler handler) {
		handler.onEvent(this);
	}

	@Getter
	private final Long id;

	@Getter
	private final PageType page;

	@Getter
	private Serializable data;

	public OpenPageEvent(Long id, PageType page) {
		this.id = id;
		this.page = page;
	}

	public OpenPageEvent(Long id, PageType page, Serializable data) {
		this.id = id;
		this.page = page;
		this.data = data;
	}

}
