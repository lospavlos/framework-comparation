package cz.vsmie.tomanek.gwt.server;

import lombok.Delegate;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import cz.vsmie.tomanek.gwt.client.calendar.ActivityService;
import cz.vsmie.tomanek.server.activity.ActivityLocal;
import cz.vsmie.tomanek.server.util.Bean;

public class ActivityServiceImpl extends RemoteServiceServlet implements
		ActivityService {
	private static final long serialVersionUID = 1L;
	
	@Delegate
	ActivityLocal contactProxy = Bean.get(ActivityLocal.class);
}
