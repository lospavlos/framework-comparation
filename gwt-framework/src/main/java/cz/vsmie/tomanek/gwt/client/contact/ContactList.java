package cz.vsmie.tomanek.gwt.client.contact;

import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.ActionCell.Delegate;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.SimplePager.TextLocation;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.google.web.bindery.event.shared.SimpleEventBus;

import cz.vsmie.tomanek.gwt.client.event.OpenPageEvent;
import cz.vsmie.tomanek.gwt.client.event.PageType;
import cz.vsmie.tomanek.gwt.client.utils.ExceptionHandler;
import cz.vsmie.tomanek.server.contact.shared.ContactTO;
import cz.vsmie.tomanek.server.util.shared.DataList;

public class ContactList extends Composite {
	ContactServiceAsync service = GWT.create(ContactService.class);
	private final static int recordsPerPage = 15;

	private static ContactListUiBinder uiBinder = GWT
			.create(ContactListUiBinder.class);

	interface ContactListUiBinder extends UiBinder<Widget, ContactList> {
	}

	@UiField
	VerticalPanel panel;

	AsyncDataProvider<ContactTO> dataGrid;

	CellTable<ContactTO> table = new CellTable<ContactTO>();
	SimplePager pager;
	SimpleEventBus mainBus;

	public ContactList(SimpleEventBus mainBus) {
		initWidget(uiBinder.createAndBindUi(this));
		this.mainBus = mainBus;
		SimplePager.Resources pagerResources = GWT
				.create(SimplePager.Resources.class);

		pager = new SimplePager(TextLocation.CENTER, pagerResources, false, 0,
				true);
		pager.setPageSize(recordsPerPage);
		pager.setDisplay(table);
		panel.add(table);
		panel.add(pager);
		this.dataGrid = new AsyncDataProvider<ContactTO>() {

			@Override
			protected void onRangeChanged(HasData<ContactTO> display) {

				final int start = display.getVisibleRange().getStart();
				double temp = start / 15.0;
				int result = (start / 15);
				if ((temp - result) > 0) {
					result++;
				}

//				Window.alert("Range: " + start + " -"
//						+ display.getVisibleRange().getLength()
//						+ ", resultPage: " + result);
				service.getContacts(result, recordsPerPage,
						new AsyncCallback<DataList<ContactTO>>() {

							@Override
							public void onSuccess(DataList<ContactTO> result) {
								updateRowCount(result.getTotalCount(), true);
								table.setRowCount(result.getTotalCount());
								updateRowData(start, result);
							}

							@Override
							public void onFailure(Throwable caught) {
								ExceptionHandler.handle(caught);
							}
						});
			}
		};
		initGrid();
		dataGrid.addDataDisplay(table);
	}

	private void initGrid() {
		Column<ContactTO, String> firstNameColumn = new Column<ContactTO, String>(
				new TextCell()) {
			@Override
			public String getValue(ContactTO object) {
				return object.getName();
			}
		};
		Column<ContactTO, String> lastNameColumn = new Column<ContactTO, String>(
				new TextCell()) {
			@Override
			public String getValue(ContactTO object) {
				return object.getSurname();
			}
		};
		Column<ContactTO, String> emailColumn = new Column<ContactTO, String>(
				new TextCell()) {
			@Override
			public String getValue(ContactTO object) {
				return object.getPrimaryEmail();
			}
		};
		Column<ContactTO, String> phoneColumn = new Column<ContactTO, String>(
				new TextCell()) {
			@Override
			public String getValue(ContactTO object) {
				return object.getWorkPhone();
			}
		};

		Column<ContactTO, ContactTO> actionViewColumn = new Column<ContactTO, ContactTO>(
				new ActionCell<ContactTO>("zobrazit", getView())) {
			@Override
			public ContactTO getValue(ContactTO object) {
				return object;
			}
		};

		Column<ContactTO, ContactTO> actionEditColumn = new Column<ContactTO, ContactTO>(
				new ActionCell<ContactTO>("upravit", getEdit())) {
			@Override
			public ContactTO getValue(ContactTO object) {
				return object;
			}
		};

		Column<ContactTO, ContactTO> actionDeleteColumn = new Column<ContactTO, ContactTO>(
				new ActionCell<ContactTO>("smazat", getDelete())) {
			@Override
			public ContactTO getValue(ContactTO object) {
				return object;
			}
		};

		table.addColumn(firstNameColumn, "Jméno");
		table.addColumn(lastNameColumn, "Příjmení");
		table.addColumn(emailColumn, "E-mail");
		table.addColumn(phoneColumn, "Pracovní telefon");
		table.addColumn(actionViewColumn);
		table.addColumn(actionEditColumn);
		table.addColumn(actionDeleteColumn);
	}

	private Delegate<ContactTO> getEdit() {
		return new Delegate<ContactTO>() {

			@Override
			public void execute(ContactTO object) {
				mainBus.fireEvent(new OpenPageEvent(object.getId(),
						PageType.CONTACT_EDIT));
			}
		};
	}

	private Delegate<ContactTO> getDelete() {
		return new Delegate<ContactTO>() {

			@Override
			public void execute(ContactTO object) {
				service.remove(object.getId(), new AsyncCallback<Void>() {

					@Override
					public void onSuccess(Void result) {
						table.setVisibleRangeAndClearData(
								table.getVisibleRange(), true);
					}

					@Override
					public void onFailure(Throwable caught) {
						ExceptionHandler.handle(caught);
					}
				});
			}
		};
	}

	private Delegate<ContactTO> getView() {
		return new Delegate<ContactTO>() {

			@Override
			public void execute(ContactTO object) {
				mainBus.fireEvent(new OpenPageEvent(object.getId(),
						PageType.CONTACT_VIEW, object));
			}
		};
	}

}
