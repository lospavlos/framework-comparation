package cz.vsmie.tomanek.gwt.client.contact;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DateLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import cz.vsmie.tomanek.server.contact.shared.ContactTO;

public class ViewContact extends Composite {

	private static ViewContactUiBinder uiBinder = GWT
			.create(ViewContactUiBinder.class);

	private ContactTO contact;

	interface ViewContactUiBinder extends UiBinder<Widget, ViewContact> {
	}

	@UiField
	Label name;
	@UiField
	Label surName;
	@UiField
	DateLabel birthday;
	@UiField
	Label note;

	@UiField
	Label street;
	@UiField
	Label city;
	@UiField
	Label postCode;
	@UiField
	Label country;
	@UiField
	Label mobilePhone;
	@UiField
	Label workPhone;
	@UiField
	Label privatePhone;
	@UiField
	Label primaryEmail;
	@UiField
	Label secondaryEmail;

	public ViewContact(ContactTO contact) {
		initWidget(uiBinder.createAndBindUi(this));
		this.contact = contact;
		initFields();
	}

	public ViewContact(Long id) {
		initWidget(uiBinder.createAndBindUi(this));
		ContactServiceAsync service = GWT.create(ContactService.class);
		service.getContact(id, new AsyncCallback<ContactTO>() {

			@Override
			public void onSuccess(ContactTO result) {
				ViewContact.this.contact = result;
				initFields();
			}

			@Override
			public void onFailure(Throwable caught) {
				ViewContact.this.contact = new ContactTO();
				// TODO inform user about failure
			}
		});
		initFields();
	}

	private void initFields() {
		this.name.setText(contact.getName());
		this.surName.setText(contact.getSurname());
		this.birthday.setValue(contact.getBirthday());
		this.note.setText(contact.getNote());
		this.street.setText(contact.getStreet());
		this.city.setText(contact.getCity());
		this.postCode.setText(contact.getPostCode());
		this.country.setText(contact.getCountry());
		this.mobilePhone.setText(contact.getMobilePhone());
		this.privatePhone.setText(contact.getPrivatePhone());
		this.workPhone.setText(contact.getWorkPhone());
		this.primaryEmail.setText(contact.getPrimaryEmail());
		this.secondaryEmail.setText(contact.getSecondaryEmail());
	}

}
