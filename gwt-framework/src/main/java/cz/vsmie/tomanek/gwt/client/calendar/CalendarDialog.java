package cz.vsmie.tomanek.gwt.client.calendar;

import lombok.val;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

import cz.vsmie.tomanek.gwt.client.utils.ExceptionHandler;
import cz.vsmie.tomanek.gwt.client.utils.RequireValidator;
import cz.vsmie.tomanek.server.activity.shared.ActivityTO;
import cz.vsmie.tomanek.server.activity.shared.Priority;

public class CalendarDialog extends Composite implements Editor<ActivityTO> {

	private static CalendarDialogUiBinder uiBinder = GWT
			.create(CalendarDialogUiBinder.class);

	interface Driver extends SimpleBeanEditorDriver<ActivityTO, CalendarDialog> {
	}

	Driver driver = GWT.create(Driver.class);

	interface CalendarDialogUiBinder extends UiBinder<Widget, CalendarDialog> {
	}

	private ActivityTO activity;
	private SaveActivityListener listener;

	@UiField
	TextBox subject;

	@UiField
	TextBox note;

	@UiField
	DateBox start;

	@UiField
	DateBox end;

	@UiField
	ListBox priority;

	RequireValidator v = new RequireValidator();

	public CalendarDialog(ActivityTO activity, SaveActivityListener listener) {
		initWidget(uiBinder.createAndBindUi(this));
		this.activity = activity;
		this.listener = listener;
		for (val priorityValue : Priority.values()) {
			priority.addItem(priorityValue.toString());
		}
		loadFromTO();
		start.getDatePicker();
		v.addInput(subject);
		v.addInput(priority);
		v.addInput(start);
		v.addInput(end);
	}

	@UiHandler("saveButton")
	void save(ClickEvent e) {
		this.convertToTO();
		if (v.checkValid()) {
			if (isValidDate()) {
				this.listener.save(activity);
			} else {
				ExceptionHandler.showErrorDialog("Neplatné datum",
						"Datum do nemůže být menší než datum od");
			}
		}
	}

	private boolean isValidDate() {
		return this.activity.getStart().before(this.activity.getEnd());
	}

	@UiHandler("cancelButton")
	void cancel(ClickEvent e) {
		this.listener.save(null);
	}

	private void loadFromTO() {
		driver.initialize(this);
		driver.edit(activity);
		if (activity.getPriority() != null) {
			for (int i = 0; i < priority.getItemCount(); i++) {
				if (priority.getItemText(i).equals(
						activity.getPriority().toString())) {
					priority.setSelectedIndex(i);
					break;
				}
			}
		}
	}

	private void convertToTO() {
		this.activity = driver.flush();
		if (this.priority.getSelectedIndex() >= 0) {
			val text = this.priority.getItemText(this.priority
					.getSelectedIndex());
			this.activity.setPriority(Priority.valueOf(text));
		}
	}
}
