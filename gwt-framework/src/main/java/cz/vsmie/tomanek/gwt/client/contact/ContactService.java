package cz.vsmie.tomanek.gwt.client.contact;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import cz.vsmie.tomanek.server.contact.shared.ContactTO;
import cz.vsmie.tomanek.server.util.shared.DataList;

@RemoteServiceRelativePath("contacts")
public interface ContactService extends RemoteService {
	public ContactTO save(ContactTO contact);

	public ContactTO getContact(Long id);

	public void remove(Long id);

	public DataList<ContactTO> getContacts(int page, int pageSize);

}
