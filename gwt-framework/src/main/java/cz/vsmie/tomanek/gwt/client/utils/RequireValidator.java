package cz.vsmie.tomanek.gwt.client.utils;

import java.util.LinkedList;
import java.util.List;

import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

public class RequireValidator {
	List<Widget> list = new LinkedList<Widget>();
	List<Widget> listEmail = new LinkedList<Widget>();

	private boolean requireTextBox(TextBox field) {
		if (field.getValue().equals("") && field.getValue().length() == 0) {
			return false;
		}
		return true;
	}

	private boolean requireListBox(ListBox box) {
		if (box.getSelectedIndex() == -1) {
			return false;
		}
		return true;
	}

	private boolean requireDatePicker(DateBox field) {
		if (field.getValue() == null) {
			return false;
		}
		return true;
	}

	private boolean requireIntegerBox(IntegerBox box) {
		if (box.getValue() == null) {
			return false;
		}
		return true;
	}

	public boolean checkValid() {
		boolean valid = true;
		for (Widget w : list) {
			if (checkValid(w) == false) {
				displayNotValid(w);
				valid = false;
			} else {
				displayValid(w);
			}
		}
		for (Widget w : listEmail) {
			if (w instanceof TextBox) {
				if (checkValid(w)) {
					if (!isValidEmail(((TextBox) w).getText())) {
						valid = false;
						displayNotValid(w);
					} else {
						displayValid(w);
					}
				}
			}
		}
		return valid;
	}

	public boolean checkValid(Widget o) {
		if (o instanceof TextBox) {
			return requireTextBox((TextBox) o);
		} else if (o instanceof DateBox) {
			return requireDatePicker((DateBox) o);
		} else if (o instanceof IntegerBox) {
			return requireIntegerBox((IntegerBox) o);
		} else if (o instanceof TextArea) {
			return requireTextArea((TextArea) o);
		} else if (o instanceof ListBox) {
			return requireListBox((ListBox) o);
		}
		return false;
	}

	public boolean requireTextArea(TextArea area) {
		if (area.getValue().length() == 0) {
			return false;
		}
		return true;
	}

	public void addInput(Widget o) {
		list.add(o);
	}

	public void addEmailInput(Widget o) {
		this.listEmail.add(o);
	}

	private void displayNotValid(Widget o) {
		o.setStyleName("valid-error", true);
	}

	private void displayValid(Widget o) {
		o.setStyleName("valid-error", false);
	}

	public boolean isValidEmail(String email) {
		return email
				.matches("^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$");
	}
}
