package cz.vsmie.tomanek.gwt.server;

import lombok.Delegate;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import cz.vsmie.tomanek.gwt.client.contact.ContactService;
import cz.vsmie.tomanek.server.contact.ContactLocal;
import cz.vsmie.tomanek.server.util.Bean;

public class ContactServiceImpl extends RemoteServiceServlet implements
		ContactService {
	private static final long serialVersionUID = 1L;
	
	@Delegate
	ContactLocal contactProxy = Bean.get(ContactLocal.class);


}
