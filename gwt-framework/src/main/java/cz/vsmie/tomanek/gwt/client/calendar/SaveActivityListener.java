package cz.vsmie.tomanek.gwt.client.calendar;

import cz.vsmie.tomanek.server.activity.shared.ActivityTO;

public interface SaveActivityListener {
	void save(ActivityTO activity);
}
