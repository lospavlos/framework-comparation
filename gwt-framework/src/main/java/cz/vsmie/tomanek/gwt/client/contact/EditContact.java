package cz.vsmie.tomanek.gwt.client.contact;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.web.bindery.event.shared.SimpleEventBus;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

import cz.vsmie.tomanek.gwt.client.event.OpenPageEvent;
import cz.vsmie.tomanek.gwt.client.event.PageType;
import cz.vsmie.tomanek.gwt.client.utils.ExceptionHandler;
import cz.vsmie.tomanek.gwt.client.utils.RequireValidator;
import cz.vsmie.tomanek.server.contact.shared.ContactTO;

public class EditContact extends Composite implements Editor<ContactTO> {

	private static EditContactUiBinder uiBinder = GWT
			.create(EditContactUiBinder.class);

	interface Driver extends SimpleBeanEditorDriver<ContactTO, EditContact> {
	}

	Driver driver = GWT.create(Driver.class);

	RequireValidator v = new RequireValidator();

	interface EditContactUiBinder extends UiBinder<Widget, EditContact> {
	}

	public EditContact(ContactTO contact, SimpleEventBus bus) {
		initWidget(uiBinder.createAndBindUi(this));
		this.bus = bus;
		this.contact = contact;
		loadFromTO();
		initValidator();
	}

	public EditContact(Long id, SimpleEventBus bus) {
		initWidget(uiBinder.createAndBindUi(this));
		ContactServiceAsync service = GWT.create(ContactService.class);
		this.bus = bus;
		initValidator();
		service.getContact(id, new AsyncCallback<ContactTO>() {

			@Override
			public void onSuccess(ContactTO result) {
				EditContact.this.contact = result;
				loadFromTO();
			}

			@Override
			public void onFailure(Throwable caught) {
				EditContact.this.contact = new ContactTO();
			}
		});
	}

	private void initValidator() {
		v.addInput(name);
		v.addInput(surname);
		v.addInput(primaryEmail);
		v.addEmailInput(primaryEmail);
		v.addEmailInput(secondaryEmail);
	}

	private ContactTO contact;
	private SimpleEventBus bus;

	@UiField
	TextBox name;
	@UiField
	TextBox surname;
	@UiField
	DateBox birthday;
	@UiField
	TextBox street;
	@UiField
	TextBox city;
	@UiField
	TextBox postCode;
	@UiField
	TextBox country;
	@UiField
	TextBox workPhone;
	@UiField
	TextBox privatePhone;
	@UiField
	TextBox mobilePhone;
	@UiField
	TextBox primaryEmail;
	@UiField
	TextBox secondaryEmail;
	@UiField
	TextArea note;

	@UiHandler("saveButton")
	void save(ClickEvent e) {
		this.convertToTO();
		if (!v.checkValid()) {
			return;
		}
		ContactServiceAsync service = GWT.create(ContactService.class);
		service.save(contact, new AsyncCallback<ContactTO>() {

			@Override
			public void onSuccess(ContactTO result) {
				bus.fireEvent(new OpenPageEvent(result.getId(),
						PageType.CONTACT_VIEW, result));
			}

			@Override
			public void onFailure(Throwable caught) {
				ExceptionHandler.handle(caught);
			}
		});
	}

	private void loadFromTO() {
		driver.initialize(this);
		driver.edit(contact);
	}

	private void convertToTO() {
		this.contact = driver.flush();
	}
}
