package cz.vsmie.tomanek.gwt.client.utils;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

public class ExceptionHandler {

	public static void handle(Throwable e) {
		if (e instanceof InvocationException) {
			showErrorDialog("Invocation error", "Ops, some error occuried");
		} else {
			showErrorDialog("Error", "Ops..");
		}
	}

	public static void showErrorDialog(String title, String description) {
		final DialogBox box = new DialogBox();
		box.setText(title);
		VerticalPanel panel = new VerticalPanel();
		panel.add(new Label(description));
		Button b = new Button();
		b.setText("close");
		b.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				box.hide();
			}
		});
		panel.add(b);
		box.add(panel);
		box.setModal(true);
		box.setGlassEnabled(true);
		box.center();
		box.show();
	}
}
