package cz.vsmie.tomanek.gwt.client.calendar;

import java.util.Date;

import lombok.val;

import com.bradrydzewski.gwt.calendar.client.Appointment;
import com.bradrydzewski.gwt.calendar.client.AppointmentStyle;
import com.bradrydzewski.gwt.calendar.client.CalendarViews;
import com.bradrydzewski.gwt.calendar.client.event.DeleteEvent;
import com.bradrydzewski.gwt.calendar.client.event.DeleteHandler;
import com.bradrydzewski.gwt.calendar.client.event.TimeBlockClickEvent;
import com.bradrydzewski.gwt.calendar.client.event.TimeBlockClickHandler;
import com.bradrydzewski.gwt.calendar.client.event.UpdateEvent;
import com.bradrydzewski.gwt.calendar.client.event.UpdateHandler;
import com.bradrydzewski.gwt.calendar.client.monthview.MonthView;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.OpenEvent;
import com.google.gwt.event.logical.shared.OpenHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DecoratedTabBar;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.CalendarUtil;

import cz.vsmie.tomanek.gwt.client.utils.ExceptionHandler;
import cz.vsmie.tomanek.server.activity.shared.ActivityTO;
import cz.vsmie.tomanek.server.util.shared.DataList;

public class Calendar extends Composite {
	ActivityServiceAsync service = GWT.create(ActivityService.class);

	private VerticalPanel panel = new VerticalPanel();

	private com.bradrydzewski.gwt.calendar.client.Calendar cal;

	public Calendar() {
		this.cal = new com.bradrydzewski.gwt.calendar.client.Calendar();
		final DecoratedTabBar bar = new DecoratedTabBar();
		HorizontalPanel hPanel = new HorizontalPanel();
		Button prev = new Button("Předchozí");
		Button today = new Button("Dnes");
		Button next = new Button("Následující");
		hPanel.add(prev);
		hPanel.add(today);
		hPanel.add(next);
		hPanel.addStyleName("left");
		prev.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				val date = CalendarUtil.copyDate(cal.getDate());
				if (cal.getView() instanceof MonthView) {
					CalendarUtil.addMonthsToDate(date, -1);
				} else {
					CalendarUtil.addDaysToDate(date, cal.getDays() * -1);
				}
				cal.setDate(date);
				bar.selectTab(bar.getSelectedTab(), true);
			}
		});
		today.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				cal.setDate(new Date());
				bar.selectTab(bar.getSelectedTab(), true);
			}
		});
		next.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				val date = CalendarUtil.copyDate(cal.getDate());
				if (cal.getView() instanceof MonthView) {
					CalendarUtil.addMonthsToDate(date, 1);
				} else {
					CalendarUtil.addDaysToDate(date, cal.getDays());
				}
				cal.setDate(date);
				bar.selectTab(bar.getSelectedTab(), true);
			}
		});

		bar.addTab("Měsíc");
		bar.addTab("Týden");
		bar.addTab("Den");
		panel.add(hPanel);
		panel.add(bar);
		bar.addSelectionHandler(new SelectionHandler<Integer>() {

			@Override
			public void onSelection(SelectionEvent<Integer> event) {
				switch (bar.getSelectedTab()) {
				case 0:
					cal.setView(CalendarViews.MONTH);
					loadData();
					break;
				case 1:
					cal.setView(CalendarViews.DAY, 7);
					loadData();
					break;
				case 2:
					cal.setView(CalendarViews.DAY, 1);
					loadData();
					break;
				}
			}
		});
		bar.selectTab(0, false);
		cal.setView(CalendarViews.MONTH);
		cal.setWidth("100%");
		cal.setHeight("500px");
		panel.add(cal);
		super.initWidget(panel);
		initData();
		loadData();
	}

	private Appointment convert(ActivityTO to) {
		Appointment ap = new Appointment();

		ap.setStart(to.getStart());
		ap.setEnd(to.getEnd());
		if (to.getId() != null) {
			ap.setId(to.getId().toString());
		}
		ap.setTitle(to.getSubject());
		ap.setDescription(to.getNote());
		if (to.getPriority() != null) {
			switch (to.getPriority()) {
			case HIGHT:
				ap.setStyle(AppointmentStyle.RED);
				break;
			case LOW:
				ap.setStyle(AppointmentStyle.GREEN);
				break;
			case MEDIUM:
				ap.setStyle(AppointmentStyle.ORANGE);
				break;
			default:
				ap.setStyle(AppointmentStyle.GREY);
			}
		}
		return ap;
	}

	private void initData() {
		cal.addOpenHandler(new OpenHandler<Appointment>() {

			@Override
			public void onOpen(OpenEvent<Appointment> event) {
				Long id = Long.parseLong(event.getTarget().getId());
				service.getActivity(id, new AsyncCallback<ActivityTO>() {

					@Override
					public void onFailure(Throwable caught) {
						ExceptionHandler.handle(caught);

					}

					@Override
					public void onSuccess(ActivityTO result) {
						createEditWindow(result);
					}
				});
			}
		});
		cal.addTimeBlockClickHandler(new TimeBlockClickHandler<Date>() {
			@Override
			public void onTimeBlockClick(TimeBlockClickEvent<Date> event) {
				createEditWindow(new ActivityTO());
			}
		});

		cal.addDeleteHandler(new DeleteHandler<Appointment>() {

			@Override
			public void onDelete(final DeleteEvent<Appointment> event) {
				service.deleteActivity(
						Long.parseLong(event.getTarget().getId()),
						new AsyncCallback<Void>() {

							@Override
							public void onFailure(Throwable caught) {
								ExceptionHandler.handle(caught);
							}

							@Override
							public void onSuccess(Void result) {
								cal.removeAppointment(event.getTarget());
							}
						});

			}
		});
		cal.addUpdateHandler(new UpdateHandler<Appointment>() {

			@Override
			public void onUpdate(final UpdateEvent<Appointment> event) {
				Long id = Long.parseLong(event.getTarget().getId());
				service.getActivity(id, new AsyncCallback<ActivityTO>() {

					@Override
					public void onFailure(Throwable caught) {
						ExceptionHandler.handle(caught);
					}

					@Override
					public void onSuccess(ActivityTO result) {
						result.setStart(event.getTarget().getStart());
						result.setEnd(event.getTarget().getEnd());
						service.saveActivity(result,
								new AsyncCallback<ActivityTO>() {

									@Override
									public void onSuccess(ActivityTO result) {
										// TODO Auto-generated method
										// stub
									}

									@Override
									public void onFailure(Throwable caught) {
										ExceptionHandler.handle(caught);
									}
								});
					}
				});
			}
		});
	}

	private void createEditWindow(ActivityTO to) {
		val box = new DialogBox();
		box.setTitle("Aktivita");
		box.add(new CalendarDialog(to, new SaveActivityListener() {
			@Override
			public void save(ActivityTO activity) {
				if (activity == null) {
					box.hide();
					return;
				}
				// val isNew = activity.getId() == null;
				service.saveActivity(activity, new AsyncCallback<ActivityTO>() {
					@Override
					public void onFailure(Throwable caught) {
						ExceptionHandler.handle(caught);
					}

					@Override
					public void onSuccess(ActivityTO result) {
						box.hide();
						loadData();
						return;
						// if (isNew) {
						// Window.alert("adding new appoitment with id"
						// + result.getId());
						// cal.addAppointment(convert(result));
						// } else {
						// Appointment found = null;
						// for (val app : cal.getAppointments()) {
						// if (app.getId().equals(result.getId() + "")) {
						// found = app;
						// break;
						// }
						// }
						// if (found != null) {
						// cal.removeAppointment(found);
						// Window.alert("removing old app.");
						// }
						// Window.alert("adding updated app.");
						// cal.addAppointment(convert(result));
						// }
					}
				});
			}
		}));
		box.setGlassEnabled(true);
		box.setModal(true);
		box.center();
		box.show();
	}

	private void loadData() {
		cal.clearAppointments();
		Date start = CalendarUtil.copyDate(cal.getDate());
		Date end = CalendarUtil.copyDate(cal.getDate());
		if (cal.getView() instanceof MonthView) {
			CalendarUtil.setToFirstDayOfMonth(start);
			CalendarUtil.setToFirstDayOfMonth(end);
			CalendarUtil.addMonthsToDate(end, 1);
		} else {
			CalendarUtil.addDaysToDate(end, cal.getDays());
		}
		service.getAllActivities(start, end,
				new AsyncCallback<DataList<ActivityTO>>() {

					@Override
					public void onSuccess(DataList<ActivityTO> result) {
						for (val activity : result) {
							cal.addAppointment(convert(activity));
						}
					}

					@Override
					public void onFailure(Throwable caught) {
						ExceptionHandler.handle(caught);
					}
				});

	}

}
