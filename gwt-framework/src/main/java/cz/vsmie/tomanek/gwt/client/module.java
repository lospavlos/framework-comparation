package cz.vsmie.tomanek.gwt.client;

import lombok.val;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DecoratedStackPanel;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.SimpleEventBus;

import cz.vsmie.tomanek.gwt.client.calendar.Calendar;
import cz.vsmie.tomanek.gwt.client.contact.ContactList;
import cz.vsmie.tomanek.gwt.client.contact.EditContact;
import cz.vsmie.tomanek.gwt.client.contact.ViewContact;
import cz.vsmie.tomanek.gwt.client.event.OpenPageEvent;
import cz.vsmie.tomanek.gwt.client.event.OpenPageHandler;
import cz.vsmie.tomanek.gwt.client.event.PageType;
import cz.vsmie.tomanek.server.contact.shared.ContactTO;

/**
 * Základní třída sloužící jako vstupní bod
 */
public class module implements EntryPoint {

	private Widget currentContent;
	private DockPanel panel;
	SimpleEventBus mainBus;

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		mainBus = new SimpleEventBus();
		generateLayout();
		mainBus.addHandler(OpenPageEvent.TYPE, new OpenPageHandler() {

			@Override
			public void onEvent(OpenPageEvent event) {
				switch (event.getPage()) {
				case CALENDAR:
					changeContent(new Calendar());
					break;
				case CONTACT_EDIT:
					if (event.getData() != null
							&& event.getData() instanceof ContactTO) {
						changeContent(new EditContact((ContactTO) event
								.getData(), mainBus));
					} else {
						if (event.getData() != null
								&& event.getData() instanceof ContactTO) {
							changeContent(new EditContact((ContactTO) event
									.getData(), mainBus));
						} else {
							changeContent(new EditContact(event.getId(),
									mainBus));
						}
					}
					break;
				case CONTACT_LIST:
					changeContent(new ContactList(mainBus));
					break;
				case CONTACT_VIEW:
					if (event.getData() != null
							&& event.getData() instanceof ContactTO) {
						changeContent(new ViewContact((ContactTO) event
								.getData()));
					} else {
						if (event.getData() != null
								&& event.getData() instanceof ContactTO) {
							changeContent(new ViewContact((ContactTO) event
									.getData()));
						} else {
							changeContent(new ViewContact(event.getId()));
						}
						break;
					}
				}
			}
		});

	}

	private void generateLayout() {
		DecoratedStackPanel stackPanel = new DecoratedStackPanel();
		stackPanel.setWidth("200px");
		VerticalPanel vPanel = new VerticalPanel();
		Button newContact = new Button();
		newContact.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				mainBus.fireEvent(new OpenPageEvent(null,
						PageType.CONTACT_EDIT, new ContactTO()));

			}
		});
		newContact.setText("Nový kontakt");
		Button contactList = new Button();
		contactList.setText("Seznam kontaktů");
		contactList.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				mainBus.fireEvent(new OpenPageEvent(null, PageType.CONTACT_LIST));

			}
		});
		Button calendar = new Button();
		calendar.setText("Kalendář");
		calendar.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				mainBus.fireEvent(new OpenPageEvent(null, PageType.CALENDAR));
			}
		});

		vPanel.add(newContact);
		vPanel.add(contactList);
		vPanel.add(calendar);
		stackPanel.add(vPanel, SafeHtmlUtils.fromString("Navigace"));

		panel = new DockPanel();
		panel.setSpacing(4);
		val title = new Label("GWT framework");
		title.setStyleName("title");
		panel.add(title, DockPanel.NORTH);
		panel.add(stackPanel, DockPanel.WEST);

		RootPanel.get().add(panel);
	}

	private void changeContent(Widget w) {
		if (currentContent != null)
			panel.remove(currentContent);

		panel.add(w, DockPanel.CENTER);
		this.currentContent = w;
	}
}
