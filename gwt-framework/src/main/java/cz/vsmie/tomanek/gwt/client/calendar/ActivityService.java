package cz.vsmie.tomanek.gwt.client.calendar;

import java.util.Date;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import cz.vsmie.tomanek.server.activity.shared.ActivityTO;
import cz.vsmie.tomanek.server.util.shared.DataList;

@RemoteServiceRelativePath("activities")
public interface ActivityService extends RemoteService {
	DataList<ActivityTO> getAllActivities(Date from, Date to);

	ActivityTO getActivity(Long id);

	ActivityTO saveActivity(ActivityTO activity);

	void deleteActivity(Long id);
}
