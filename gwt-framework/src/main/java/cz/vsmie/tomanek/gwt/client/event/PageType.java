package cz.vsmie.tomanek.gwt.client.event;

public enum PageType {
	CONTACT_VIEW, CONTACT_EDIT, CONTACT_LIST, CALENDAR

}
