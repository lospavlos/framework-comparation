package cz.vsmie.tomanek.primefaces.contact;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import lombok.Delegate;
import lombok.val;
import lombok.extern.log4j.Log4j;
import cz.vsmie.tomanek.server.contact.ContactLocal;
import cz.vsmie.tomanek.server.contact.shared.ContactTO;
import cz.vsmie.tomanek.server.util.Bean;

@ManagedBean(name = "ContactEditBean")
@ViewScoped
@Log4j
public class ContactEditBean implements Serializable {
	private static final long serialVersionUID = 1L;

	@Delegate
	private ContactTO contact;

	public ContactEditBean() {
		val param = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestParameterMap();
		if (param.containsKey("id")) {
			try {
				val id = Long.parseLong(param.get("id"));
				this.contact = Bean.get(ContactLocal.class).getContact(id);
			} catch (NumberFormatException e) {
				log.debug("Id parameter: " + param.get("id")
						+ " is not valid for contact edit", e);
			}
		}
		if (contact == null) {
			contact = new ContactTO();
		}
	}

	public String save() {
		val saved = Bean.get(ContactLocal.class).save(contact);
		try {
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("contact-view.xhtml?id=" + saved.getId());
		} catch (IOException e) {
		}
		return "contact-view.xhtml?id=" + saved.getId();
	}

}
