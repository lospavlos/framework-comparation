package cz.vsmie.tomanek.primefaces.calendar;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import lombok.Getter;
import lombok.Setter;
import lombok.val;

import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyScheduleModel;
import org.primefaces.model.ScheduleModel;

import cz.vsmie.tomanek.server.activity.ActivityLocal;
import cz.vsmie.tomanek.server.activity.shared.ActivityTO;
import cz.vsmie.tomanek.server.activity.shared.Priority;
import cz.vsmie.tomanek.server.util.Bean;

@ManagedBean(name = "scheduleController")
@ViewScoped
public class CalendarController implements Serializable {
	private static final long serialVersionUID = 1L;

	@Getter
	private ScheduleModel eventModel;

	@Getter
	@Setter
	private ActivityCalendarEvent event = new ActivityCalendarEvent(
			new ActivityTO());

	public CalendarController() {
		eventModel = new LazyScheduleModel() {
			private static final long serialVersionUID = 1L;

			@Override
			public void loadEvents(Date start, Date end) {
				val activities = Bean.get(ActivityLocal.class)
						.getAllActivities(start, end);

				for (val activity : activities) {
					super.addEvent(new ActivityCalendarEvent(activity));
				}
			}
		};
	}

	public String getPriority_() {
		if (event.getActivity().getPriority() == null) {
			return null;
		}
		return event.getActivity().getPriority().toString();
	}

	public void setPriority_(String priority) {
		this.event.getActivity().setPriority(Priority.valueOf(priority));
	}

	public Date getRandomDate(Date base) {
		Calendar date = Calendar.getInstance();
		date.setTime(base);
		date.add(Calendar.DATE, ((int) (Math.random() * 30)) + 1);
		return date.getTime();
	}

	public Date getInitialDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(calendar.get(Calendar.YEAR), Calendar.FEBRUARY,
				calendar.get(Calendar.DATE), 0, 0, 0);

		return calendar.getTime();
	}

	public void addEvent(ActionEvent actionEvent) {
		val activity = Bean.get(ActivityLocal.class).saveActivity(
				event.getActivity());
		event.setActivity(activity);

		if (event.getId() == null)
			eventModel.addEvent(event);
		else
			eventModel.updateEvent(event);

		event = new ActivityCalendarEvent(new ActivityTO());
	}

	public void onEventSelect(SelectEvent selectEvent) {
		event = (ActivityCalendarEvent) selectEvent.getObject();
	}

	public void onDateSelect(SelectEvent selectEvent) {
		event = new ActivityCalendarEvent(new ActivityTO());
		event.setStartDate((Date) selectEvent.getObject());
		event.setEndDate((Date) selectEvent.getObject());
	}

	public void onEventMove(ScheduleEntryMoveEvent event) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Event moved", "Day delta:" + event.getDayDelta()
						+ ", Minute delta:" + event.getMinuteDelta());

		addMessage(message);
	}

	public void onEventResize(ScheduleEntryResizeEvent event) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Event resized", "Day delta:" + event.getDayDelta()
						+ ", Minute delta:" + event.getMinuteDelta());

		addMessage(message);
	}

	private void addMessage(FacesMessage message) {
		// testing purpose only
		// FacesContext.getCurrentInstance().addMessage(null, message);
	}

}
