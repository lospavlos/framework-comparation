package cz.vsmie.tomanek.primefaces.contact;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import lombok.Getter;
import cz.vsmie.tomanek.server.contact.ContactLocal;
import cz.vsmie.tomanek.server.util.Bean;

@ManagedBean(name = "ContactListBean")
@ViewScoped
public class ContactListBean {

	@Getter
	private ContactLazyModel gridModel = new ContactLazyModel();


	public void delete(Long deleteId) {
		Bean.get(ContactLocal.class).remove(deleteId);
	}

}
