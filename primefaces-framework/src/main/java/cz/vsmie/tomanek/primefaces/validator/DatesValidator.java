package cz.vsmie.tomanek.primefaces.validator;

import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("twoDates")
public class DatesValidator implements Validator {

	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		Date compared = (Date) value;

		Date secondDate = (Date) component.getAttributes().get("otherDate");
		if (compared == null || secondDate == null) {
			return;
		}
		if (compared.after(secondDate)) {
			((UIInput) component).setValid(false);
			throw new ValidatorException(new FacesMessage(
					"Datum od je větší než datum do."));
		}
	}

}
