package cz.vsmie.tomanek.primefaces.contact;

import java.util.List;
import java.util.Map;

import lombok.val;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;
import org.primefaces.model.SortOrder;

import cz.vsmie.tomanek.server.contact.ContactLocal;
import cz.vsmie.tomanek.server.contact.shared.ContactTO;
import cz.vsmie.tomanek.server.util.Bean;

public class ContactLazyModel extends LazyDataModel<ContactTO> {
	private static final long serialVersionUID = 1L;

	public ContactLazyModel() {
		load(1, 1, null, null);
	}

	@Override
	public ContactTO getRowData(String rowKey) {
		return Bean.get(ContactLocal.class).getContact(Long.parseLong(rowKey));
	}

	@Override
	public Object getRowKey(ContactTO object) {
		return object.getId();
	}

	@Override
	public List<ContactTO> load(int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, String> filters) {
		return load(first, pageSize, null, null);
	}

	@Override
	public List<ContactTO> load(int first, int pageSize,
			List<SortMeta> multiSortMeta, Map<String, String> filters) {

		int page = first / pageSize;
		val result = Bean.get(ContactLocal.class).getContacts(page, pageSize);

		this.setRowCount(result.getTotalCount());
		return result.getData();
	}
}
