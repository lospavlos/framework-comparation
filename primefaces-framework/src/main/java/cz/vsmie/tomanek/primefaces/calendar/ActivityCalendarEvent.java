package cz.vsmie.tomanek.primefaces.calendar;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import org.primefaces.model.DefaultScheduleEvent;

import cz.vsmie.tomanek.server.activity.shared.ActivityTO;

@AllArgsConstructor
public class ActivityCalendarEvent extends DefaultScheduleEvent {
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	private ActivityTO activity;

	public Object getData() {
		return this.activity;
	}

	public String getTitle() {
		return activity.getSubject();
	}

	public void setTitle(String title) {
		activity.setSubject(title);
	}

	public Date getStartDate() {
		return activity.getStart();
	}

	public void setStartDate(Date date) {
		this.activity.setStart(date);
	}

	public Date getEndDate() {
		return activity.getEnd();
	}

	public void setEndDate(Date date) {
		this.activity.setEnd(date);
	}

	public boolean isAllDay() {
		// TODO Auto-generated method stub
		return false;
	}

	public String getStyleClass() {
		return "";
	}

	public boolean isEditable() {
		return true;
	}

	public void setNote(String note) {
		this.activity.setNote(note);
	}

	public String getNote() {
		return this.activity.getNote();
	}

}
