package cz.vsmie.tomanek.server.activity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import cz.vsmie.tomanek.server.activity.shared.Priority;
import cz.vsmie.tomanek.server.util.shared.AbstractEntity;

@Getter
@Setter
@Entity
@Table(name = "activity")
public class Activity extends AbstractEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize = 1, name = "ACTIVITY_ID_GENERATOR")
	@GeneratedValue(generator = "ACTIVITY_ID_GENERATOR")
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	private Date start;

	@Temporal(TemporalType.TIMESTAMP)
	private Date end;

	private String subject;
	private String note;

	private Priority priority;

	@Override
	public String getReferenceText() {
		return subject;
	}
}
