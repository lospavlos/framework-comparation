package cz.vsmie.tomanek.server.contact.shared;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContactTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;

	private String name;
	private String surname;
	private Date birthday;

	private String note;

	// address
	private String street;
	private String city;
	private String postCode;
	private String country;

	// phones
	private String mobilePhone;
	private String workPhone;
	private String privatePhone;

	// emails
	private String primaryEmail;
	private String secondaryEmail;

}
