package cz.vsmie.tomanek.server.contact;

import javax.ejb.Remote;

import cz.vsmie.tomanek.server.contact.shared.ContactTO;
import cz.vsmie.tomanek.server.util.shared.DataList;

@Remote
public interface ContactRemote {
	public ContactTO getContact(Long id);

	public ContactTO save(ContactTO contact);

	public DataList<ContactTO> getContacts(int page, int pageSize);
	
	public void remove(Long id);
}
