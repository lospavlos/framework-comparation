package cz.vsmie.tomanek.server.util;

import java.text.MessageFormat;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import cz.vsmie.tomanek.server.contact.ContactFakeService;
import cz.vsmie.tomanek.server.contact.ContactLocal;

public class Bean {
	private static boolean fakeData = false;
	private static ContactLocal fakeContacts = new ContactFakeService();

	public static <T> T get(Class<T> clazz) {
		return getBean(clazz);
	}

	@SuppressWarnings("unchecked")
	public static <T> T getBean(final Class<T> beanClass) {
		if (fakeData && beanClass.equals(ContactLocal.class)) {
			return (T) fakeContacts;
		}
		return getRealBean(beanClass);
	}

	@SuppressWarnings("unchecked")
	private static <T> T getRealBean(final Class<T> beanClass) {
		try {
			final InitialContext context = new InitialContext();

			final String application = (String) context
					.lookup("java:app/AppName"); //$NON-NLS-1$

			final String name = MessageFormat
					.format("java:global/{0}/{1}!{2}", application, beanClass.getSimpleName().replaceFirst("Remote$", "Service").replaceFirst("Local$", "Service"), beanClass.getName()); //$NON-NLS-1$ //$NON-NLS-2$

			return (T) context.lookup(name);
		} catch (final NamingException e) {
			throw new IllegalStateException(e);
		}
	}
}
