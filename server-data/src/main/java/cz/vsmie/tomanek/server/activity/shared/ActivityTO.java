package cz.vsmie.tomanek.server.activity.shared;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ActivityTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;

	private Date start;

	private Date end;

	private String subject;

	private String note;

	private Priority priority;
}
