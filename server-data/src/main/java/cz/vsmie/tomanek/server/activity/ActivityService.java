package cz.vsmie.tomanek.server.activity;

import java.util.Date;
import java.util.LinkedList;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;

import lombok.val;
import cz.vsmie.tomanek.server.activity.shared.ActivityTO;
import cz.vsmie.tomanek.server.util.DateUtil;
import cz.vsmie.tomanek.server.util.ReflectionConvertor;
import cz.vsmie.tomanek.server.util.shared.DataList;

@Stateless
@Local(ActivityLocal.class)
@Remote(ActivityRemote.class)
public class ActivityService implements ActivityLocal {

	@PersistenceUnit(unitName = "FRAMEWORKS-JPA")
	private EntityManagerFactory emFactory;

	@PersistenceContext
	private EntityManager em;

	@Override
	public DataList<ActivityTO> getAllActivities(Date from, Date to) {
		Date start = DateUtil.getStartDate(from);
		Date end = DateUtil.getEndDate(to);

		val em = emFactory.createEntityManager();
		val query = em.getCriteriaBuilder().createQuery(Activity.class);
		val fromQuery = query.from(Activity.class);
		val where = query.where(
				em.getCriteriaBuilder().lessThanOrEqualTo(
						fromQuery.get("start").as(Date.class), end),
				em.getCriteriaBuilder().greaterThanOrEqualTo(
						fromQuery.get("end").as(Date.class), start));
		val q = em.createQuery(query);
		val result = q.getResultList();

		val list = new LinkedList<ActivityTO>();
		for (val item : result) {
			list.add(ReflectionConvertor.copyValues(item, ActivityTO.class));
		}

		return new DataList<>(list, getCount(start, end));
	}

	@Override
	public ActivityTO getActivity(Long id) {
		val activity = em.find(Activity.class, id);
		if (activity == null) {
			return null;
		}
		return ReflectionConvertor.copyValues(activity, ActivityTO.class);
	}

	@Override
	public ActivityTO saveActivity(ActivityTO activity) {
		val activityEntity = ReflectionConvertor.copyValues(activity,
				Activity.class);
		val em = emFactory.createEntityManager();
		if (activityEntity.getId() == null) {
			em.persist(activityEntity);
			em.flush();
			return ReflectionConvertor.copyValues(activityEntity,
					ActivityTO.class);
		} else {
			val persisted = em.merge(activityEntity);
			em.flush();
			return ReflectionConvertor.copyValues(persisted, ActivityTO.class);
		}
	}

	@Override
	public void deleteActivity(Long id) {
		em.remove(em.find(Activity.class, id));
	}

	private int getCount(Date start, Date end) {
		val query = em.getCriteriaBuilder().createQuery(Long.class);
		val from = query.from(Activity.class);
		query.select(em.getCriteriaBuilder().count(from));
		val where = query.where(
				em.getCriteriaBuilder().lessThanOrEqualTo(
						from.get("start").as(Date.class), end),
				em.getCriteriaBuilder().greaterThanOrEqualTo(
						from.get("end").as(Date.class), start));
		val q = em.createQuery(query);
		return q.getSingleResult().intValue();
	}
}
