package cz.vsmie.tomanek.server.util;

import java.lang.reflect.InvocationTargetException;

import lombok.val;
import lombok.extern.log4j.Log4j;
import cz.vsmie.tomanek.server.contact.Contact;
import cz.vsmie.tomanek.server.contact.shared.ContactTO;

@Log4j
/**
 * 
 * @author Pavel Tománek
 * @version 0.1
 *
 */
public class ReflectionConvertor {
	/**
	 * Copy fields between two objects @ * @param source object with data
	 * 
	 * @param target
	 *            type of object where copy data to
	 * @return
	 */
	public static <T> T copyValues(Object source, Class<T> target) {
		T instance;
		try {
			instance = target.getConstructor().newInstance();
		} catch (InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e4) {
			log.error("Invalid constructor!", e4);
			return null;
		}
		for (val field : source.getClass().getDeclaredFields()) {
			try {
				String name = field.getName();
				name = Character.toUpperCase(name.charAt(0))
						+ name.substring(1);
				val sourceMethod = source.getClass().getMethod("get" + name);
				val value = sourceMethod.invoke(source);
				val targetMethod = instance.getClass().getMethod("set" + name,
						sourceMethod.getReturnType());
				targetMethod.invoke(instance, value);
			} catch (IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException
					| SecurityException e1) {
				log.error("Unable to copy field" + field.getName() + " from "
						+ source.getClass().getCanonicalName() + " to "
						+ target.getCanonicalName(), e1);

			}
		}
		return instance;
	}

	public static void main(String[] params) throws InstantiationException,
			IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, NoSuchMethodException, SecurityException {
		Contact c = new Contact();
		c.setName("aaa");
		val to = copyValues(c, ContactTO.class);
		System.out.println(to.getName());
	}
}
