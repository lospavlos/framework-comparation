package cz.vsmie.tomanek.server.util;

import static java.util.Calendar.HOUR_OF_DAY;
import static java.util.Calendar.MILLISECOND;
import static java.util.Calendar.MINUTE;
import static java.util.Calendar.SECOND;

import java.util.Calendar;
import java.util.Date;

import lombok.val;

public class DateUtil {

	public static Date getStartDate(Date date) {
		val cal = Calendar.getInstance();
		cal.setTime(date);

		cal.set(MILLISECOND, 0);
		cal.set(SECOND, 0);
		cal.set(MINUTE, 0);
		cal.set(HOUR_OF_DAY, 0);

		return cal.getTime();
	}

	public static Date getEndDate(Date date) {
		val cal = Calendar.getInstance();
		cal.setTime(date);

		cal.set(MILLISECOND, 999);
		cal.set(SECOND, 59);
		cal.set(MINUTE, 59);
		cal.set(HOUR_OF_DAY, 23);

		return cal.getTime();
	}
}
