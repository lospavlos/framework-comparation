package cz.vsmie.tomanek.server.util.shared;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Delegate;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@AllArgsConstructor
@Getter
public class DataList<T> implements List<T>, Serializable {
	private static final long serialVersionUID = 1L;

	@Delegate
	private List<T> data;

	private int totalCount;
	
	public DataList(){
		super();
	}
}

