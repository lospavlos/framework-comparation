package cz.vsmie.tomanek.server.activity;

import java.util.Date;

import javax.ejb.Remote;

import cz.vsmie.tomanek.server.activity.shared.ActivityTO;
import cz.vsmie.tomanek.server.util.shared.DataList;

@Remote
public interface ActivityRemote {
	DataList<ActivityTO> getAllActivities(Date from, Date to);

	ActivityTO getActivity(Long id);

	ActivityTO saveActivity(ActivityTO activity); // TODO: replace with activityTo

	void deleteActivity(Long id);
}
