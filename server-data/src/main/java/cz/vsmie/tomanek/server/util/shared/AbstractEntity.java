package cz.vsmie.tomanek.server.util.shared;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AbstractEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public abstract Long getId();
	public abstract void setId(Long id);
	public abstract String getReferenceText();
}
