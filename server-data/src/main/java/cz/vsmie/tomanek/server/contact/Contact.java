package cz.vsmie.tomanek.server.contact;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "contact")
public class Contact {

	@Id
	@SequenceGenerator(allocationSize = 1, name = "CONTACT_ID_GENERATOR")
	@GeneratedValue(generator = "CONTACT_ID_GENERATOR")
	private Long id;

	private String name;
	private String surname;
	@Temporal(TemporalType.DATE)
	private Date birthday;

	private String note;

	// address
	private String street;
	private String city;
	private String postCode;
	private String country;

	// phones
	private String mobilePhone;
	private String workPhone;
	private String privatePhone;

	// emails
	private String primaryEmail;
	private String secondaryEmail;
}
