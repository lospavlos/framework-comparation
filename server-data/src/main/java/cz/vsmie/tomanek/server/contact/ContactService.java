package cz.vsmie.tomanek.server.contact;

import java.util.LinkedList;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;

import lombok.val;
import cz.vsmie.tomanek.server.contact.shared.ContactTO;
import cz.vsmie.tomanek.server.util.ReflectionConvertor;
import cz.vsmie.tomanek.server.util.shared.DataList;

@Stateless
@Local(ContactLocal.class)
@Remote(ContactRemote.class)
public class ContactService implements ContactLocal {

	@PersistenceUnit(unitName = "FRAMEWORKS-JPA")
	private EntityManagerFactory emFactory;

	@PersistenceContext
	private EntityManager em;

	@Override
	public ContactTO getContact(Long id) {
		val contact = em.find(Contact.class, id);
		if (contact == null) {
			return null;
		}
		return ReflectionConvertor.copyValues(contact, ContactTO.class);
	}

	@Override
	public ContactTO save(ContactTO contact) {
		val contactEntity = ReflectionConvertor.copyValues(contact,
				Contact.class);
		val em = emFactory.createEntityManager();
		if (contactEntity.getId() == null) {
			em.persist(contactEntity);
			em.flush();
			return ReflectionConvertor.copyValues(contactEntity,
					ContactTO.class);
		} else {
			val persisted = em.merge(contactEntity);
			em.flush();
			return ReflectionConvertor.copyValues(persisted, ContactTO.class);
		}
	}

	public void remove(Long id) {
		em.remove(em.find(Contact.class, id));
	}

	@Override
	public DataList<ContactTO> getContacts(int page, int pageSize) {
		val em = emFactory.createEntityManager();
		val query = em.getCriteriaBuilder().createQuery(Contact.class);
		val q = em.createQuery(query);
		q.setFirstResult(page * pageSize);
		q.setMaxResults(pageSize);
		val result = q.getResultList();

		val list = new LinkedList<ContactTO>();
		for (val item : result) {
			list.add(ReflectionConvertor.copyValues(item, ContactTO.class));
		}

		return new DataList<>(list, getCount());
	}

	private int getCount() {
		val em = emFactory.createEntityManager();
		val query = em.getCriteriaBuilder().createQuery(Long.class);
		val from = query.from(Contact.class);
		query.select(em.getCriteriaBuilder().count(from));
		val q = em.createQuery(query);
		return q.getSingleResult().intValue();
	}
}
