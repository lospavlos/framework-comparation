package cz.vsmie.tomanek.server.contact;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import lombok.val;
import cz.vsmie.tomanek.server.contact.shared.ContactTO;
import cz.vsmie.tomanek.server.util.shared.DataList;

public class ContactFakeService implements ContactLocal {

	private int totalRecords = 10000;

	private static ContactTO[] contact;

	static {
		contact = new ContactTO[20];
		for (int i = 0; i < 20; i++) {
			val c = new ContactTO();
			c.setId((long) i);
			c.setCity(getRandomString());
			c.setStreet(getRandomString());
			c.setName(getRandomString());
			c.setSurname(getRandomString());
			c.setPrimaryEmail(getRandomString());
			c.setPrivatePhone(getRandomString());
			c.setWorkPhone(getRandomString());
			contact[i] = c;
		}
	}

	private static String getRandomString() {
		char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < 10; i++) {
			char c = chars[random.nextInt(chars.length)];
			sb.append(c);
		}
		return sb.toString();

	}

	@Override
	public ContactTO getContact(Long id) {
		return contact[new Random().nextInt(20)];
	}

	@Override
	public ContactTO save(ContactTO contact) {
		return contact;
	}

	@Override
	public DataList<ContactTO> getContacts(int page, int pageSize) {
		List<ContactTO> list = new LinkedList<>();
		for (int i = 0; i < pageSize; i++) {
			list.add(contact[((page * pageSize) + i) % 20]);
		}
		return new DataList<>(list, totalRecords);
	}

	@Override
	public void remove(Long id) {
	}
}
