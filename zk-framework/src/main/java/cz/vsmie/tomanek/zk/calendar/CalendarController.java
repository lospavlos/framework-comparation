package cz.vsmie.tomanek.zk.calendar;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import lombok.val;

import org.zkoss.calendar.Calendars;
import org.zkoss.calendar.api.CalendarEvent;
import org.zkoss.calendar.event.CalendarsEvent;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Window;

import cz.vsmie.tomanek.server.activity.ActivityLocal;
import cz.vsmie.tomanek.server.activity.shared.ActivityTO;
import cz.vsmie.tomanek.server.util.Bean;

public class CalendarController extends GenericForwardComposer<Window> {
	private static final long serialVersionUID = 1L;

	private static final String dialog = "/WEB-INF/composite/calendarDialog.zul";

	private Calendars calendar;
	private CalendarModel model;

	public CalendarController() {
		this.model = new CalendarModel();
	}

	@Override
	public void doAfterCompose(Window comp) throws Exception {
		super.doAfterCompose(comp);
		calendar.setModel(model);
	}

	public void onClick$today() {
		calendar.setCurrentDate(new Date());
	}

	public void onClick$prev() {
		calendar.previousPage();
	}

	public void onClick$next() {
		calendar.nextPage();
	}

	public void onClick$pageDay() {
		calendar.setMold("default");
		calendar.setDays(1);
	}

	public void onClick$pageWeek() {
		calendar.setMold("default");
		calendar.setDays(7);
	}

	public void onClick$pageMonth() {
		calendar.setMold("month");
	}

	public void onEventCreate$calendar(CalendarsEvent event) {
		processCreateEdit(event);
	}

	public void onEventEdit$calendar(CalendarsEvent event) {
		processCreateEdit(event);
	}

	private void processCreateEdit(final CalendarsEvent event) {
		event.stopClearGhost();
		ActivityCalendarsEvent data = (ActivityCalendarsEvent) event
				.getCalendarEvent();

		if (data == null) {
			data = new ActivityCalendarsEvent(new ActivityTO());
			data.setHeaderColor("#3366ff");
			data.setContentColor("#6699ff");
			data.setBeginDate(event.getBeginDate());
			data.setEndDate(event.getEndDate());
		}

		Map<String, Object> param = new HashMap<>();
		param.put("activity", data);
		val comp = Executions.createComponents(dialog, null, param);
		comp.addEventListener("onSaveActity", new EventListener<Event>() {

			@Override
			public void onEvent(Event event2) throws Exception {
				val data = (ActivityCalendarsEvent) event2.getData();
				if (data == null) {
					event.clearGhost();
					return;
				}
				boolean isNew = data.getId() == null;
				data.setActivity(save(data.getActivity()));
				if (isNew) {
					model.add(data);
				} else {
					model.update(data);
				}
			}
		});
		((Window) comp).doModal();
	}

	public void onEventUpdate$calendar(CalendarsEvent event) {
		ActivityCalendarsEvent data = (ActivityCalendarsEvent) event
				.getCalendarEvent();
		data.setBeginDate(event.getBeginDate());
		data.setEndDate(event.getEndDate());
		data.setActivity(save(data.getActivity()));
		model.update(data);
	}

	private ActivityTO save(ActivityTO activity) {
		return Bean.get(ActivityLocal.class).saveActivity(activity);
	}
}
