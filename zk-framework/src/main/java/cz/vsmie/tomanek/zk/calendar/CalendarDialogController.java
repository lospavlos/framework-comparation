package cz.vsmie.tomanek.zk.calendar;

import java.util.List;

import lombok.Getter;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;

import cz.vsmie.tomanek.server.activity.shared.Priority;

/**
 * Ovladač pro dialogové okno.
 * 
 * @author pavel
 * 
 */
public class CalendarDialogController extends GenericForwardComposer<Window> {
	private static final long serialVersionUID = 1L;

	@Getter
	private ActivityCalendarsEvent activity;

	@Getter
	private ListModel<Priority> priorityModel = new ListModelArray<>(
			Priority.values());

	public CalendarDialogController() {
		this.activity = (ActivityCalendarsEvent) Executions.getCurrent()
				.getArg().get("activity");
	}

	public void onClick$saveButton() {
		check(self);
		if (activity.getBeginDate().after(activity.getEndDate())) {
			return;
		}
		Events.postEvent("onSaveActity", self, activity);
		self.detach();
	}

	public void onClick$cancelButton() {
		Events.postEvent("onSaveActity", self, null);
		self.detach();
	}

	private void check(Component component) {
		checkIsValid(component);

		List<Component> children = component.getChildren();
		for (Component each : children) {
			check(each);
		}
	}

	private void checkIsValid(Component component) {
		if (component instanceof InputElement) {
			if (!((InputElement) component).isValid()) {
				// Force show errorMessage
				((InputElement) component).getText();
			}
		}
	}
}
