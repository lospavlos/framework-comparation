package cz.vsmie.tomanek.zk.contact;

import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Window;

public class ListContactController extends GenericForwardComposer<Window> {
	private static final long serialVersionUID = 1L;

	private Grid contactDataGrid;

	@Override
	public void doAfterCompose(Window comp) throws Exception {
		super.doAfterCompose(comp);
		contactDataGrid.setModel(new ContactLiveModel());
		contactDataGrid.setRowRenderer(new ContactRowRenderer());
	}

}
