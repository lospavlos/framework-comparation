package cz.vsmie.tomanek.zk.contact;

import java.util.List;

import lombok.val;

import org.zkoss.zul.ListModelList;

import cz.vsmie.tomanek.server.contact.ContactLocal;
import cz.vsmie.tomanek.server.contact.shared.ContactTO;
import cz.vsmie.tomanek.server.util.Bean;

public class ContactLiveModel extends ListModelList<ContactTO> {
	private static final long serialVersionUID = 1L;

	private int pageSize = 20;
	private int currentPage = -1;

	private List<ContactTO> data;
	private int totalSize;

	public ContactLiveModel() {
		loadDataForPage(0);
	}

	@Override
	public int getSize() {
		return this.totalSize;
	}

	@Override
	public ContactTO getElementAt(int j) {
		if (j < this.currentPage * this.pageSize
				|| j >= (this.currentPage + 1) * this.pageSize) {
			loadDataForPage(j / this.pageSize);
		}
		return data.get(j - (currentPage * pageSize));
	}

	private void loadDataForPage(int page) {
		this.currentPage = page;
		val serverData = Bean.get(ContactLocal.class).getContacts(currentPage,
				pageSize);
		this.data = serverData.getData();
		this.totalSize = serverData.getTotalCount();
	}

}
