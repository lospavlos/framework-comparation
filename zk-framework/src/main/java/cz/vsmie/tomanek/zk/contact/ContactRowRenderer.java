package cz.vsmie.tomanek.zk.contact;

import lombok.val;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.MouseEvent;
import org.zkoss.zul.A;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Label;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;

import cz.vsmie.tomanek.server.contact.ContactLocal;
import cz.vsmie.tomanek.server.contact.shared.ContactTO;
import cz.vsmie.tomanek.server.util.Bean;

/**
 * Třída zabývající se generováním jednotlivých řádků do tabulky se seznamem
 * kontaktů. Kromě využití RowRenderer lze použít obdobný přístup jako u
 * PrimeFaces tedy zapsání všech potřebných informací přímo do šablony. K tomu
 * slouží element template
 * 
 * @author pavel
 * 
 */
public class ContactRowRenderer implements RowRenderer<ContactTO> {

	@Override
	public void render(Row row, ContactTO data, int index) throws Exception {
		new Label(data.getName()).setParent(row);
		new Label(data.getSurname()).setParent(row);
		new Label(data.getPrimaryEmail()).setParent(row);
		new Label(data.getWorkPhone()).setParent(row);
		new Label(data.getPrivatePhone()).setParent(row);
		createActions(row, data);
	}

	private void createActions(Row row, final ContactTO data) {
		val container = new Hlayout();
		container.setHflex("1");

		val edit = new A("uprav");
		edit.setHref("contact-edit.zul?id=" + data.getId());

		val view = new A("zobraz");
		view.setHref("contact-view.zul?id=" + data.getId());

		val delete = new A("smaž");
		delete.addEventListener(Events.ON_CLICK,
				new EventListener<MouseEvent>() {

					@Override
					public void onEvent(MouseEvent event) throws Exception {
						Bean.get(ContactLocal.class).remove(data.getId());
						refreshModel();
					}
				});

		container.appendChild(view);
		container.appendChild(edit);
		container.appendChild(delete);
		container.setParent(row);
	}

	private void refreshModel() {
		Executions.sendRedirect(null);
	}

}
