package cz.vsmie.tomanek.zk.contact;

import java.util.List;

import lombok.Getter;
import lombok.val;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;

import cz.vsmie.tomanek.server.contact.ContactLocal;
import cz.vsmie.tomanek.server.contact.shared.ContactTO;
import cz.vsmie.tomanek.server.util.Bean;

/**
 * Ovladač pro edit stránku s kontaktem. Jedná se o MVC přístup.
 * GenericForwardComposer poskytuje automatické vkládání instancí a propojení
 * událostí s metodamy. Pokud tedy definujeme Button tlacitko, composer se
 * pokusí najít tlačítko s id tlacitko a pokud ho nalezne vloží jeho instanci do
 * proměné. Obdobným způsobem funguje i párování metod. Tedy metoda
 * onClick$saveButton se automaticky spustí když na tlačítku saveButton proběhne
 * onClick událost. Kromě automatického párování lze využít manuální párování s
 * SelectorComposer a @Wire
 * 
 * @author pavel
 * 
 */

public class EditContactController extends GenericForwardComposer<Window> {
	private static final long serialVersionUID = 1L;

	private static final String redirectPage = "contact-list.zul";

	@Getter
	private ContactTO contact;
	private Component c;

	public EditContactController() {
		val param = Executions.getCurrent().getParameter("id");
		if (param != null) {
			try {
				val id = Long.parseLong(param);
				this.contact = Bean.get(ContactLocal.class).getContact(id);
			} catch (NumberFormatException e) {
				Executions.getCurrent().sendRedirect(redirectPage);
			}
		}
		if (this.contact == null) {
			contact = new ContactTO();
		}
	}

	@Override
	public void doAfterCompose(Window comp) throws Exception {
		super.doAfterCompose(comp);
		this.c = comp;
	}

	public void onClick$saveButton() {
		check(c);
		val saved = Bean.get(ContactLocal.class).save(contact);
		Executions.getCurrent().sendRedirect(
				"contact-view.zul?id=" + saved.getId());
	}

	public void onClick$cancelButton() {
		Executions.getCurrent().sendRedirect("contact-list.zul");
	}

	private void check(Component component) {
		checkIsValid(component);

		List<Component> children = component.getChildren();
		for (Component each : children) {
			check(each);
		}
	}

	private void checkIsValid(Component component) {
		if (component instanceof InputElement) {
			if (!((InputElement) component).isValid()) {
				((InputElement) component).getText();
			}
		}
	}
}
