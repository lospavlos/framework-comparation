package cz.vsmie.tomanek.zk.calendar;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import lombok.val;

import org.zkoss.calendar.api.CalendarEvent;
import org.zkoss.calendar.api.RenderContext;
import org.zkoss.calendar.impl.SimpleCalendarModel;

import cz.vsmie.tomanek.server.activity.ActivityLocal;
import cz.vsmie.tomanek.server.activity.shared.ActivityTO;
import cz.vsmie.tomanek.server.util.Bean;

public class CalendarModel extends SimpleCalendarModel {
	private static final long serialVersionUID = 1L;

	@Override
	public List get(Date beginDate, Date endDate, RenderContext rc) {
		val activities = Bean.get(ActivityLocal.class).getAllActivities(
				beginDate, endDate);
		List<CalendarEvent> list = new LinkedList<>();
		for (val activity : activities) {
			list.add(toEvent(activity));
		}
		super._list = list;
		return list;
	}

	public static ActivityCalendarsEvent toEvent(ActivityTO activity) {
		val event = new ActivityCalendarsEvent(activity);
		event.setBeginDate(activity.getStart());
		event.setEndDate(activity.getEnd());
		event.setContent(activity.getSubject());
		return event;
	}
}
