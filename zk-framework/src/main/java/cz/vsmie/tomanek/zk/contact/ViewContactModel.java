package cz.vsmie.tomanek.zk.contact;

import lombok.Delegate;
import lombok.val;

import org.zkoss.bind.annotation.Command;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.Messagebox;

import cz.vsmie.tomanek.server.contact.ContactLocal;
import cz.vsmie.tomanek.server.contact.shared.ContactTO;
import cz.vsmie.tomanek.server.util.Bean;

/**
 * Ukázka přístupu pomocí MVVM. Tedy model s daty, pomocnými modely pro tabulky,
 * ...
 * 
 * @author pavel
 * 
 */
public class ViewContactModel {

	@Delegate
	private ContactTO contact;

	public ViewContactModel() {
		val idString = Executions.getCurrent().getParameter("id");
		if (idString == null) {
			showList();
		}
		try {
			val id = Long.parseLong(idString);
			this.contact = Bean.get(ContactLocal.class).getContact(id);
		} catch (NumberFormatException e) {
			Executions.getCurrent().sendRedirect("contact-list.zul");
		}
		if (this.contact == null) {
			this.contact = new ContactTO();
			Messagebox.show("Nenalezen žádný kontakt se zvoleným id");
		}
	}

	@Command
	public void showList() {
		Executions.getCurrent().sendRedirect("contact-list.zul");
	}

}
