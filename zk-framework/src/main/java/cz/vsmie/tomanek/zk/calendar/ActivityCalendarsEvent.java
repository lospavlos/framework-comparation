package cz.vsmie.tomanek.zk.calendar;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Delegate;
import lombok.Getter;
import lombok.Setter;

import org.zkoss.calendar.impl.SimpleCalendarEvent;

import cz.vsmie.tomanek.server.activity.shared.ActivityTO;

/**
 * Události zobrazující se v kalendáři
 * @author pavel
 *
 */
@AllArgsConstructor
public class ActivityCalendarsEvent extends SimpleCalendarEvent {
	private static final long serialVersionUID = 1L;

	@Delegate
	@Getter
	@Setter
	private ActivityTO activity;

	@Override
	public void setEndDate(Date endDate) {
		super.setEndDate(endDate);
		this.activity.setEnd(endDate);
	}

	@Override
	public void setBeginDate(Date beginDate) {
		super.setBeginDate(beginDate);
		this.activity.setStart(beginDate);
	}

	@Override
	public String getContentColor() {
		if (activity.getPriority() != null) {
			switch (activity.getPriority()) {
			case HIGHT:
				return "red";
			case LOW:
				return "green";
			case MEDIUM:
				return "orange";
			default:
				return super.getContentColor();
			}
		}
		return super.getContentColor();
	}

	@Override
	public String getHeaderColor() {
		return getContentColor();
	}
}
